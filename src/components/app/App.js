import React, {Component} from 'react';
import Layout from '../Layout';
import $ from "jquery";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = { resumeData: {} };
    }

    getResumeData() {
        $.ajax({
            url:'/resumeSiteData.json',
            dataType:'json',
            cache: false,
            success: function(data){
                this.setState({ resumeSiteData: data });
            }.bind(this),
            error: function(xhr, status, err) {
                console.log(err);
                alert(err);
            }
        });
    }

    componentDidMount() {
        this.getResumeData();
    }

    render() {
        // If we don't get the JSON data, then send this
        if (!this.state.resumeSiteData) {
            return (<div>Failed to get JSON data.</div>);
        }

        return (<Layout resumeData={ this.state.resumeSiteData } />);
    }
}

export default App;