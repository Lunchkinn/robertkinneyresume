import React, { Component } from 'react';
import SocialMedia from "../socialMedia/SocialMedia";
import './Footer.css';

class Footer extends Component {
    linkTitle;
    constructor(props) {
        super(props);
        this.state = {
            socialMediaLinks: props.socialMediaLinks,
        };
    }

    render() {
        return (
            <div className={"row center footerTest"}>
                <SocialMedia socialMediaLinks={ this.props.socialMediaLinks } />
            </div>
        )
    }
}

export default Footer;