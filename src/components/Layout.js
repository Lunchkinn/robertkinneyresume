import React, {Component} from 'react';
import Navigation from '../components/navigation/Navigation';
import AboutMe from './aboutMe/AboutMe';
import Work from './work/Work';
import Skills from './skills/Skills';
import Footer from "./Footer/Footer";
import '../components/main.css';
import '../components/simple-grid.css';

class Layout extends Component {
    skillsSectionArray;
    constructor(props) {
        super(props);
        this.state = {
            resumeData: props.resumeData ? props.resumeData : {},
        };
    }

    render() {
        if (this.state.resumeData) {
            return (
                <div className={ "body-content" }>
                    <Navigation navBarItems={ this.state.resumeData.navBarItems} />

                    <AboutMe aboutMeSection={ this.state.resumeData.aboutMeSection } socialMediaLinks={ this.state.resumeData.socialMediaLinks}/>

                    <Work workSectionArray={ this.state.resumeData.workSectionArray }/>

                    <Skills skillSectionArray={ this.state.resumeData.skillsSectionArray }/>

                    <Footer socialMediaLinks={ this.state.resumeData.socialMediaLinks} />
                </div>
            );
        } else {
            return (
                <div>
                    There was a serious problem with getting the JSON data... Please refresh.
                </div>
            );
        }

    }
}
export default Layout;
