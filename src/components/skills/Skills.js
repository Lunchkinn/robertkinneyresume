import React, { Component } from 'react';
import './Skills.css'

class Skills extends Component {
    skillSection;

    constructor(props) {
        super(props);
        this.state = {
            skillSectionArray: props.skillSectionArray,
        };
    }

    render() {
        return (
            <div className={ "skillsSectionClass" } >
                <div className={ "row" }>
                    <div className={ "col-12" }>
                        <h1 className={ "center" } id={ "skillsSection" }>Skills</h1>
                    </div>
                </div>

                {this.state.skillSectionArray.map((skillSection) => {
                    return (
                        <SkillPointContainer
                            key={ skillSection.id }
                            skills={ skillSection.skills }
                            sectionName={ skillSection.skillSection }
                        />
                    );
                })}
            </div>
        );
    }
}

class SkillPointContainer extends Component {
    sectionName;

    render() {
        return (
            <div className={ "row" }>
                <div className={"col-2 hidden-sm"}/>

                <div className={ "col-3" }>
                    <h3 className={ "left top-text no-bottom-margin" }>
                        { this.props.sectionName }
                    </h3>
                </div>

                <SkillList skills={ this.props.skills } />

                <div className={"col-2 hidden-sm"}/>
            </div>
        );
    }
}

class SkillList extends Component {
    skills;
    skill;

    render() {
        return (
            <div className={ "col-5" }>
                <ul className={ "skillList top-text" }>
                    {Object.entries(this.props.skills).map(([index, skill]) => {
                        return (<li key={ "skillsSkillListItem-" + index }>{ skill.skill }</li>);
                    })}
                </ul>
            </div>
        );
    }
}

export default Skills;