import React, { Component } from 'react';
import robert from '../../images/Robert2020.jpg';
import SocialMedia from "../socialMedia/SocialMedia";
import './AboutMe.css';


class AboutMe extends Component {
    aboutMeText;
    constructor(props) {
        super(props);
        this.state = {
            aboutMeSection: props.aboutMeSection ? props.aboutMeSection : {},
            socialMediaLinks: props.socialMediaLinks ? props.socialMediaLinks : {}
        };
    }

    render() {
        return (
            <div key={ "classAboutMe" } className={ "row" }>
                <div className={"col-2 hidden-sm"}/>
                <AboutMePicture socialMediaLinks={ this.props.socialMediaLinks }/>
                <AboutMeText aboutMeData={ this.props.aboutMeSection }/>
                <div className={"col-2 hidden-sm"}/>
            </div>
        );
    }
}

class AboutMePicture extends Component {
    render() {
        return (
            <div className={ "col-3 center" }>
                <img src={ robert } className={ "aboutMePicture" } alt={ "sorry" }/>

                <div className={ "center" }>
                    <h3 className={ "socialMediaHeader" }>rkinney333@gmail.com</h3>
                </div>

                <div className={ "center" }>
                    <SocialMedia socialMediaLinks={ this.props.socialMediaLinks } />
                </div>
            </div>
        );
    }
}

class AboutMeText extends Component {
    aboutMeData;
    render() {
        return (
            <div className={ "col-5" }>
                <h1 className={ "center m-bottom" }>
                    { this.props.aboutMeData.header }
                </h1>

                <p>
                    { this.props.aboutMeData.aboutMeText }
                </p>
            </div>
        );
    }
}

export default AboutMe;