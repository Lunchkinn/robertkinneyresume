import React, { Component } from 'react';
import './Work.css';

class Work extends Component {
    constructor(props) {
        super(props);
        this.state = {
            workSectionArray: props.workSectionArray,
        };
    }

    render() {
        return (
            <div key={ "classWork" } className={ "workSection" }>
                <div className={ "row" }>
                    <div className={ "col-12" }>
                        <h1 className={ "center" } id={ "resumeSectionID" }>Work</h1>
                    </div>
                </div>

                {this.props.workSectionArray.map((workSection) => {
                    return (<WorkPlaceContainer key={ workSection.id } workSection={ workSection }/>);
                })}
            </div>
        );
    }
}

class WorkPlaceContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            workSection: props.workSection,
        };
    }

    workPlace;
    dates;
    workSection;
    jobTitle;

    render() {
        return (
            <div className={ "row" }>
                <div className={"col-2 hidden-sm"}/>

                <WorkPlaceInformation key={ this.props.workSection.workPlace } workSection={ this.props.workSection } />

                <WorkTasksContainer tasks={ this.props.workSection.tasks } jobTitle={ this.props.workSection.jobTitle }/>

                <div className={"col-2 hidden-sm"}/>
            </div>
        );
    }
}

class WorkPlaceInformation extends Component {
    render() {
        return (
            <div className={ "col-3" }>
                <h3 className={ "left top-text" }>
                    <a href={this.props.workSection.website}>{ this.props.workSection.workPlace }</a>
                </h3>

                { this.props.workSection.dates }
            </div>
        );
    }
}

class WorkTasksContainer extends Component {
    tasks;

    render() {
        return (
            <div className={ "col-5" }>
                <h4 className={ "top-text" }>{ this.props.jobTitle}</h4>
                <ul className={ "skillList" }>
                    {this.props.tasks.map((task) => {
                        return (<li key={ task.id }> { task.task }</li>);
                    })}
                </ul>
            </div>
        );
    }
}

export default Work;