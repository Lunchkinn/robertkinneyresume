import React, { Component } from 'react';
import { Link, animateScroll as scroll } from "react-scroll";
import './Navigation.css';

class Navigation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            navBarItems: props.navBarItems,
        };
    }

    render() {
        return (
            <div>
                <div key={ "navigationBar" } id={ "navigationRow" } className={ "row right" }>
                    <div className={ "col-2 hidden-sm" } />

                    <NavigationListContainer navBarItems={ this.state.navBarItems }/>

                    <div className={ "col-2 hidden-sm" } />
                </div>

                <div id={ "navigationBuffer" }/>
            </div>
        )
    }
}

class NavigationListContainer extends Component {
    scrollToTop = function() {
        return scroll.scrollToTop();
    };

    render() {
        return (
            <div className={ "col-8 navigation" }>
                <ul id={ "navigationListID" }>
                    <li className={ "center" }>
                        <a onClick={ this.scrollToTop } >
                            Home
                        </a>
                    </li>

                    {this.props.navBarItems.map((navBarItem) => {
                        return (<NavigationItem key={ navBarItem.linkTitle } link={ navBarItem.link } linkTitle={ navBarItem.linkTitle }/>);
                    })}
                </ul>
            </div>
        );
    }
}

class NavigationItem extends Component {
    render() {
        return (
            <li className={ "center" }>
                <Link
                    activeClass={ "active" }
                    to={ this.props.link }
                    smooth={ true }
                    duration={ 500 }
                    className={"navLink linkBackground"}
                    offset={-70}
                >
                    { this.props.linkTitle }
                </Link>
            </li>
        );
    }
}

export default Navigation;