import React, { Component } from 'react';
import './SocialMedia.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebook, faTwitter, faLinkedin, faGitlab } from "@fortawesome/free-brands-svg-icons";

class SocialMedia extends Component {
    linkTitle;
    twitter;
    linkedIn;
    gitLab;
    facebook;

    constructor(props) {
        super(props);
        this.state = {
            socialMediaLinks: props.socialMediaLinks,
        };
    }

    render() {
        return (
            <div>
                <ul className={ "socialMediaList center" }>
                    <li>
                        <div>
                            <a href={ this.props.socialMediaLinks.facebook } >
                                <FontAwesomeIcon icon={faFacebook} size={'3x'} />
                            </a>
                        </div>
                    </li>

                    <li>
                        <div>
                            <a href={ this.props.socialMediaLinks.twitter } >
                                <FontAwesomeIcon icon={faTwitter} size={'3x'} />
                            </a>
                        </div>
                    </li>

                    <li>
                        <div>
                            <a href={ this.props.socialMediaLinks.linkedIn } >
                                <FontAwesomeIcon icon={faLinkedin} size={'3x'} />
                            </a>
                        </div>
                    </li>
                    
                    <li>
                        <div>
                            <a href={ this.props.socialMediaLinks.gitLab } >
                                <FontAwesomeIcon icon={faGitlab} size={'3x'} />
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        )
    }
}

export default SocialMedia;